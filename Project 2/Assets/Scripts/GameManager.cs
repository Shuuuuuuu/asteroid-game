﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int numberOfAsteroids;   //Current number of Asteroids in scenes.
    public int levelNumber = 1;
    public GameObject asteroid;
    public AlienScript alien;

    public void UpdateNumberOfAsteroids(int change)
    {
        numberOfAsteroids += change;

        //Check to see if we have any asteroids left
        if(numberOfAsteroids <= 0)
        {
            //Start new level
            Invoke("StartNewLevel", 3f); 
        }
    }

    void StartNewLevel()
    {
        levelNumber++;

        //Spawn New Asterois
        for (int i = 0; i < levelNumber*2; i++)
        {
            Vector2 spawnPosition = new Vector2(Random.Range(-16.76f, 16.76f),13.13f);
            Instantiate(asteroid, spawnPosition, Quaternion.identity);
            numberOfAsteroids++;
        }

        //Setup Alien
        alien.NewLevel();
    }
}
